package br.edu.unisep.helpdesk.data.entity;


import lombok.Data;


import javax.persistence.*;
import java.time.LocalDate;


@Data
@Entity
@Table(name = "tickets")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ticketId")
    private Integer id;

    @Column(name = "nameIssuer")
    private String nameIssuer;

    @Column(name = "emailIssuer")
    private String emailIssuer;

    @Column(name = "nameResponder")
    private String nameResponder;

    @Column(name = "emailResponder")
    private String emailResponder;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "openDate")
    private LocalDate openDate;
    
    @Column(name = "status")
    private Integer status;

    @Column(name = "closeDate")
    private LocalDate closeDate;
}
