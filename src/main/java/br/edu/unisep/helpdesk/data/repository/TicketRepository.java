package br.edu.unisep.helpdesk.data.repository;

import br.edu.unisep.helpdesk.data.entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Integer> {

    List<Ticket> findByNameIssuer(String nameIssue);
    List<Ticket> findByNameResponder(String nameResponder);

}
