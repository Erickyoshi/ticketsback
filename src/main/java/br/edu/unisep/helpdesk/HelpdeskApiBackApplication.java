package br.edu.unisep.helpdesk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelpdeskApiBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelpdeskApiBackApplication.class, args);
	}

}
