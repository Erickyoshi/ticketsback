package br.edu.unisep.helpdesk.controllerTickets;

import br.edu.unisep.helpdesk.domain.dto.ticket.RegisterTicketDto;
import br.edu.unisep.helpdesk.domain.dto.ticket.TicketDto;
import br.edu.unisep.helpdesk.domain.usecase.ticket.FindAllTicketsUseCase;
import br.edu.unisep.helpdesk.domain.usecase.ticket.FindTicketByNameIssueUseCase;
import br.edu.unisep.helpdesk.domain.usecase.ticket.FindTicketByNameResponderUseCase;
import br.edu.unisep.helpdesk.domain.usecase.ticket.RegisterTicketUseCase;
import br.edu.unisep.helpdesk.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@AllArgsConstructor
@RestController
@RequestMapping("/tickets")
public class TicketsController {

    private final FindAllTicketsUseCase findAllTickets;
    private final RegisterTicketUseCase registerTicket;
    private final FindTicketByNameIssueUseCase findTicketByNameIssue;
    private final FindTicketByNameResponderUseCase findTicketByNameResponder;


    @GetMapping
    public ResponseEntity< DefaultResponse<List<TicketDto>>> findAll(){
        var tickets =findAllTickets.execute();

        return  tickets.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(DefaultResponse.of(tickets));
    }

    @PostMapping
    public ResponseEntity<DefaultResponse<Boolean>>save(@RequestBody RegisterTicketDto ticket){
        registerTicket.execute(ticket);
        return ResponseEntity.ok(DefaultResponse.of(true));
    }

    @GetMapping("/{name_issuer}")
    public ResponseEntity<DefaultResponse<List<TicketDto>>> findTicketByNameIssue(@PathVariable("name_issuer") String nameIssuer){
        var ticket = findTicketByNameIssue.execute(nameIssuer);
        return ticket == null ?
                ResponseEntity.notFound().build():
                ResponseEntity.ok(DefaultResponse.of(ticket));
    }

    @GetMapping("/{name_responder}")
    public ResponseEntity<DefaultResponse<List<TicketDto>>> findTicketByNameResponder(@PathVariable("name_responder") String nameResponder){
        var ticket = findTicketByNameResponder.execute(nameResponder);

        return ticket==null ?
                ResponseEntity.notFound().build():
                ResponseEntity.ok(DefaultResponse.of(ticket));
    }


}
