package br.edu.unisep.helpdesk.domain.usecase.ticket;

import br.edu.unisep.helpdesk.data.repository.TicketRepository;
import br.edu.unisep.helpdesk.domain.builder.ticket.TicketBuilder;
import br.edu.unisep.helpdesk.domain.dto.ticket.UpdateTicketDto;
import br.edu.unisep.helpdesk.domain.validator.ticket.UpdateTicketValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UpdateTicketUseCase {


    private final UpdateTicketValidator validator;
    private final TicketBuilder builder;
    private final TicketRepository ticketRepository;

    public void execute(UpdateTicketDto updateTicket){
        validator.validate(updateTicket);

        var ticket = builder.from(updateTicket);
        ticketRepository.save(ticket);
    }

}
