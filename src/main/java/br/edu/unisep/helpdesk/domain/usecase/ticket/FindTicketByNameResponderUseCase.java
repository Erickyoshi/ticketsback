package br.edu.unisep.helpdesk.domain.usecase.ticket;

import br.edu.unisep.helpdesk.data.repository.TicketRepository;
import br.edu.unisep.helpdesk.domain.builder.ticket.TicketBuilder;
import br.edu.unisep.helpdesk.domain.dto.ticket.TicketDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class FindTicketByNameResponderUseCase {

    private final TicketRepository ticketRepository;
    private final TicketBuilder builder;

    public List<TicketDto> execute(String nameResponder){
        var tickets = ticketRepository.findByNameResponder(nameResponder);
        return builder.from(tickets);
    }
}
