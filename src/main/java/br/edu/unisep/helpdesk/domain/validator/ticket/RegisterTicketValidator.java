package br.edu.unisep.helpdesk.domain.validator.ticket;


import br.edu.unisep.helpdesk.domain.dto.ticket.RegisterTicketDto;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.helpdesk.domain.validator.ValidationMessages.*;

@Component
public class RegisterTicketValidator {

    public void validate(RegisterTicketDto registerTicket){
        Validate.notBlank(registerTicket.getName_issuer(), MESSAGE_REQUIRED_TICKET_NAME_ISSUER);
        Validate.notBlank(registerTicket.getEmail_issuer(), MESSAGE_REQUIRED_TICKET_EMAIL_ISSUER);
        Validate.notBlank(registerTicket.getName_responder(), MESSAGE_REQUIRED_TICKET_NAME_RESPONDER);
        Validate.notBlank(registerTicket.getEmail_responder(), MESSAGE_REQUIRED_TICKET_EMAIL_RESPONDER);
        Validate.notBlank(registerTicket.getTitle(), MESSAGE_REQUIRED_TICKET_TITLE);
        Validate.notBlank(registerTicket.getDescription(), MESSAGE_REQUIRED_TICKET_DESCRIPTION);

    }
}
