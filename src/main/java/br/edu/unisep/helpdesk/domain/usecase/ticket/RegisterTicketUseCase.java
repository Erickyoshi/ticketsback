package br.edu.unisep.helpdesk.domain.usecase.ticket;


import br.edu.unisep.helpdesk.data.repository.TicketRepository;
import br.edu.unisep.helpdesk.domain.builder.ticket.TicketBuilder;
import br.edu.unisep.helpdesk.domain.dto.ticket.RegisterTicketDto;
import br.edu.unisep.helpdesk.domain.validator.ticket.RegisterTicketValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@AllArgsConstructor
public class RegisterTicketUseCase {

    private final RegisterTicketValidator validator;
    private final TicketBuilder builder;
    private final TicketRepository ticketRepository;

    public void execute(RegisterTicketDto registerTicket){
        validator.validate(registerTicket);
        registerTicket.setStatus(1);
        registerTicket.setOpen_date(LocalDate.now());

        var ticket = builder.from(registerTicket);

        ticketRepository.save(ticket);
    }
}
