package br.edu.unisep.helpdesk.domain.validator.ticket;


import br.edu.unisep.helpdesk.domain.dto.ticket.UpdateTicketDto;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.helpdesk.domain.validator.ValidationMessages.MESSAGE_REQUIRED_TICKET_STATUS;

@Component
@AllArgsConstructor
public class UpdateTicketValidator {

    private final RegisterTicketValidator registerTicket;

    public void validate(UpdateTicketDto ticket){
        Validate.isTrue(ticket.getStatus() == 2, MESSAGE_REQUIRED_TICKET_STATUS);
}
}
