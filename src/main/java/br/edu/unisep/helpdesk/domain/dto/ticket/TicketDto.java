package br.edu.unisep.helpdesk.domain.dto.ticket;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class TicketDto {

    private Integer id;

    private String name_issuer;

    private String email_issuer;

    private String name_responder;

    private String email_responder;

    private String title;

    private String description;

    private LocalDate open_date;

    private Integer status;

    private LocalDate close_date;
}
