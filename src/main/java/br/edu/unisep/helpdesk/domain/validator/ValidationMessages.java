package br.edu.unisep.helpdesk.domain.validator;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidationMessages {


    public static final String MESSAGE_REQUIRED_TICKET_ID = "Informe o id do ticket.";
    public static final String MESSAGE_REQUIRED_TICKET_NAME_ISSUER = "Informe o nome do emissor.";
    public static final String MESSAGE_REQUIRED_TICKET_EMAIL_ISSUER = "Informe o e-mail do emissor.";
    public static final String MESSAGE_REQUIRED_TICKET_NAME_RESPONDER = "Informe o nome do responsável.";
    public static final String MESSAGE_REQUIRED_TICKET_EMAIL_RESPONDER = "Informe o e-mail do respónsável.";
    public static final String MESSAGE_REQUIRED_TICKET_TITLE = "Informe o título do ticket";
    public static final String MESSAGE_REQUIRED_TICKET_DESCRIPTION = "Informe a descrição do ticket.";
    public static final String MESSAGE_REQUIRED_TICKET_STATUS = "O status já está fechado, não é possível realizar mais atualizações.";



}
