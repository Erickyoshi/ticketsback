package br.edu.unisep.helpdesk.domain.builder.ticket;


import br.edu.unisep.helpdesk.data.entity.Ticket;
import br.edu.unisep.helpdesk.domain.dto.ticket.RegisterTicketDto;
import br.edu.unisep.helpdesk.domain.dto.ticket.TicketDto;
import br.edu.unisep.helpdesk.domain.dto.ticket.UpdateTicketDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TicketBuilder {

    public List<TicketDto> from(List<Ticket> tickets) {return tickets.stream().map(this::from).collect(Collectors.toList());}

    public TicketDto from(Ticket ticket){
        return new TicketDto(
                ticket.getId(),
                ticket.getNameIssuer(),
                ticket.getEmailIssuer(),
                ticket.getNameResponder(),
                ticket.getEmailResponder(),
                ticket.getTitle(),
                ticket.getDescription(),
                ticket.getOpenDate(),
                ticket.getStatus(),
                ticket.getCloseDate()
        );
    }

    public Ticket from(UpdateTicketDto updateTicket){

            Ticket ticket = from((UpdateTicketDto) updateTicket);
            ticket.setId(updateTicket.getId());
            return ticket;
    }

    public Ticket from(RegisterTicketDto registerTicketDto){
        Ticket ticket = new Ticket();
        ticket.setNameIssuer(registerTicketDto.getName_issuer());
        ticket.setEmailIssuer(registerTicketDto.getEmail_issuer());
        ticket.setNameResponder(registerTicketDto.getName_responder());
        ticket.setEmailResponder(registerTicketDto.getEmail_responder());
        ticket.setTitle(registerTicketDto.getTitle());
        ticket.setDescription(registerTicketDto.getDescription());
        ticket.setOpenDate(registerTicketDto.getOpen_date());
        ticket.setStatus(registerTicketDto.getStatus());
        ticket.setCloseDate(registerTicketDto.getClose_date());

        return ticket;
    }


}
