package br.edu.unisep.helpdesk.domain.dto.ticket;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateTicketDto extends RegisterTicketDto {

    private Integer id;

}
